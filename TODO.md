TODO

Major tasks
- Integrate with calendar
- User interface for adding/editing rooms. Save to db
- Support more room types (quiet rooms etc)
- Room usage statistics
- Possibility to add/edit people
- Hue plugin
- 3d version for board


Calendar MVP
- Show booking status for rooms
- Allow people to book room from app
