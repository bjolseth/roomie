#!/usr/bin/env node
/* eslint-env node */

const path = require('path');
const shell = require('shelljs');
const program = require('commander');

const transforms = '[ babelify --presets [ es2015 react ] --plugins [ transform-object-rest-spread ] ]';

function watchify(entry, output) {
  shell.exec(`./node_modules/.bin/watchify --debug -t ${transforms} ${entry} -o ${output}`,
    { async: true });
}

function sass(dir, watchIt = true) {
  const watch = watchIt ? '--watch ' : '';
  shell.exec(`./node_modules/.bin/node-sass ${watch} ${dir}/css -o ${dir}/build --source-map true`, { async: true });
}

function watch(dir) {
  const entry = path.join(dir, 'client/index.js');
  const output = path.join(dir, 'build/bundle.js');
  watchify(entry, output);
  sass(dir);
}

const build = (subdir) => {
  const dir = path.resolve(__dirname, subdir);
  if (!shell.test('-d', dir)) {
    console.log('dir not found:', dir);
    shell.exit(1);
  }

  shell.mkdir(path.resolve(dir, 'build'));
  console.log('Compile: ', dir);
  sass(dir, false);
  shell.exec(`./node_modules/.bin/browserify --debug -t ${transforms} ${dir}/client/index.js -o ${dir}/build/bundle.js`);
};

program
  .command('help', { isDefault: true, noHelp: true })
  .description('display help text')
  .action(() => { program.outputHelp(); });

program
  .command('build')
  .description('Create bundle js ')
  .option('-d, --dir [dir]', 'directory')
  .action(options => build(options.dir));

program
  .command('watch')
  .description('Start web server, monitor js and css files and build incrementally as they change')
  .option('-d, --dir [dir]', 'directory')
  .action((options) => {
    // build it first
    build(options.dir);
    const dir = path.resolve(__dirname, options.dir);
    if (!shell.test('-d', dir)) {
      console.log('directory not found:', dir);
      shell.exit(1);
    }

    shell.mkdir(path.resolve(dir, 'build'));
    console.log('Watch: ', dir);
    watch(dir);
  });


// program
//   .command('bundle')
//   .description('Make distributable (binary) bundle')
//   .action(() => {
//     const BundleDir = 'bundle';
//     shell.mkdir((path.resolve(__dirname, BundleDir)));
//     shell.exec(`enclose src/index.js -o ${BundleDir}/App`);
//     shell.cp('index.html', BundleDir);
//     shell.cp('-R', 'assets', BundleDir);
//     shell.cp('-R', 'build', BundleDir);
//   });

program
  .command('clean')
  .description('Remove all built files')
  .action(() => {
    shell.rm('-rf', './build');
    shell.rm('-rf', './bundle');
  });

program.parse(process.argv);
