Setup Roomie
----------------------------------

![Roomie Mobile Web Client](./assets/demo.jpg)

Roomie connects to Cisco Video systems and report whether there are people in the meeting rooms, based on sensors in the video systems such as face count and ultrasonic motion detection.

The room statuses are shown in map screens on a mobile web client.

It consists of a Node-based web server that connects to video endpoints and use their APIs, and a React-based web client.

This guide describes how to download, setup and configure roomie for your own offices. You can add your own floors and map images, and add rooms and connection data to each floor.

## Requirements:

- a computer with git and npm / node installed (laptop, raspberry pi)
- cisco video endpoints with integrator user access (spark or on-prem)


## Build and configure server

```
cd roomie
npm install
```

Configure the system for your video systems:

```
cd server
cp config.json.example config.json
```

Edit config.json and add the video endpoints you want to use. It is recommended to add integrator users to the video systems and use these for roomie, not admin or remote support users.

NOTE: When you edit JSON files, be aware that the JSON format is pretty picky, compared to normal Javascript. Common mistakes:

- JSON only accepts "", not ''
- You must also have "" around the object key, not just the value
- The last element in an array or object cannot have a dangling comma

Valid:

`{ "room": "kitchen", "floor": 3 }`

Invalid (breaks all the rules above):

`{'room': 'kitchen', floor: 3, }`

#### Start the server

From the roomie root folder:

```
npm start
```

Verify in the console that the server is able to connect to the endpoints.


## Build client and configure client

```
npm run build-client
```

Configure the client:

Edit the file `client/config.json` and add the rooms you want. x and y are percentages and should be 0-100, left to right and top to bottom. Make sure the room name is the same in the client and server config files.

You can change the floors and the images used by the floors by modifying the config file. To change the height of the floor images, modify the `.floor { height: 100px }` attribute in css/style.scss and rebuild the client

If you want to modify the client and have it built automatically, run:

```
npm run watch-client
```

This will rebuild the client fast every time you change a JS or CSS file.

If you don't want to show the people tab in the client, set `showPeople: false` in the client config file

## Open the client

Using your phone or a laptop, access the roomie server by typing
`<ip-of-your-server>:3333` in the url field

Hopefully the rooms should now show correct room status.
