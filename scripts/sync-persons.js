const fs = require('fs');
const spark = require('../server/sparkAdapter');

// Connecting Norway:
const room = '7f3886b0-b8e5-11e4-81e4-bd25093145aa';
const token = 'YTc4OWI3ZjgtM2U3Ny00YmVjLWI0NjMtZjQzOTEzYTJhYzYyY2Y4YWEwZDctMjBk';

let personList;
const file = '../db/persons.json';


function addMemberDetails(person) {
  const email = person.emails[0];
  if (person.type !== 'person' || !email.includes('cisco.com')) return;
  const data = {
    avatar: person.avatar,
    name: person.displayName,
    email,
    created: person.created,
  };
  personList.push(data);
}

function finish() {
  fs.writeFileSync(file, JSON.stringify(personList, null, 2));
}


function addNext(list, token) {
  if (i++ > MaxPersons || !list.length) {
    finish();
    return;
  }
  const item = list.pop();
  spark.findUser(item.personId, token).then(data => {
    console.log('add', item.personEmail);
    addMemberDetails(data);
    addNext(list, token);
  });
}


function findDude(email) {
  return personList.findIndex(p => p.email === email) > -1;
}

function syncPerson(dude) {
  // console.log('sync', dude.personEmail);
  if (!findDude(dude.personEmail)) console.log('NEW:', dude);
}

function getAllMembers() {
  spark.getPeople(room, token)
  .then((data) => {
    const list = data.items;
    // console.log(list[0]);
    list.forEach((dude) => { syncPerson(dude); });
    personList.forEach((dude) => {
      if (list.findIndex(p => p.personEmail === dude.email) < 0) {
        console.log('LOST:', dude.email);
      }
    });
    // addNext(list, token);
    // console.log(JSON.stringify(list, null, 2));
  });
  // .catch(e => console.error('Oooops', e));
}

function init() {
  personList = JSON.parse(fs.readFileSync(file));
  getAllMembers(token);
  // console.log(personList[0]);
}

init();
