const React = require('react');

const MapMarker = require('./MapMarker');
const config = require('./config');

const Floor = (props) => {
  const rooms = props.rooms || [];
  const filter = props.roomFilter;
  const markers = rooms.map(room => {
    const noData = room.available === null;
    const status = noData ? 'unknown' : (room.available ? 'available' : 'unavailable');
    const x = room.x;
    const y = room.y;
    const above = room.nameAbove ? 1 : 0;
    const noRooms = filter && !room.name.toLowerCase().includes(filter);
    if (noRooms) return null;

    return (
      <MapMarker
        key={room.name}
        name={room.name}
        x={x}
        y={y}
        status={status}
        above={above}
      />
    );
  });

  const roomCount = markers.filter(m => m !== null).length;
  if (roomCount < 1) return null;
  const floor = config.floors[props.floor];
  const style = { backgroundImage: `url(${floor.map})` };

  return (
    <div className="floor-box">
      <div className="floor-name">{floor.name}</div>
      <div className="floor" style={style}>
        {markers}
      </div>
    </div>
  );
};

module.exports = Floor;
