const React = require('react');

import { AreaChart, Area, ResponsiveContainer, XAxis } from 'recharts';


const Table = (props) => (
  <ResponsiveContainer height={60} width="75%">
    <AreaChart data={props.values} margin={{ top: 10, bottom: 0, left: 5, right: 5 }}>
      <Area
        isAnimationActive={false}
        type="monotone"
        dataKey="inUse"
        stroke="black"
        fill="#ff7133"
        type="step"
      />
      <XAxis dataKey="time" interval={10} tick={{ fontSize: '10px' }} />
    </AreaChart>
  </ResponsiveContainer>
);

const Stats = (props) => {
  const rooms = props.getStats().map((room, i) => (
    <div className="row" key={i} >
      <div className="name">{room.name}</div>
      <Table values={room.inUse} />
    </div>
  ));
  return <div className="stats scrollable">{rooms}</div>;
};

module.exports = Stats;
