const roomCount = 15;
const samples = 100;

function randomSet(n) {
  const r = [];
  let inUse = 0;
  let count = 0;
  while (r.length < n) {
    if (Math.random() > 0.9) inUse = inUse > 0 ? 0 : 1;
    // inUse = parseInt(r.length / 20);
    const time = parseInt(8 + (r.length / 10));
    r.push({ time, inUse });
  }
  return r;
}

module.exports = function randomData() {
  const rooms = Array(roomCount).fill(1).map((t, i) => {
    const name = 'Heyerdahl ' + i;
    return { name, inUse: randomSet(samples) };
  });
  return rooms;
}
