const React = require('react');

const Intro = (props) => {
  return (
    <div className="help-popup">
      <div className="image" />
      <div className="text">
        <b>Roomie</b> shows if a meeting room is available or not LIVE, based on
        sensors in the Cisco video systems, such as face detector and ultrasonic radar.
      </div>
      <div className="status">
        <div className="row">
          <div className="ball available" /> Empty (no people detected)
        </div>
        <div className="row">
          <div className="ball unavailable" /> In use (people detected)
        </div>
        <div className="row">
          <div className="ball unknown" /> Unknown (no data access)
        </div>
      </div>
      <button>OK!</button>
    </div>
  );
};

module.exports = Intro;
