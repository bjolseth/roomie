const React = require('react');

const Legend = (props) => {
  return (
    <div className="legend">
      <div className="available ball" />
      <div>Available</div>
      <div className="unavailable ball" />
      <div>In use</div>
      <div className="unknown ball" />
      <div>Unknown</div>

    </div>
  );
}

module.exports = Legend;
