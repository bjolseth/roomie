const React = require('react');
const PropTypes = require('prop-types');

const Label = require('./Label');

const MapW = 340;
const MapH = 81;
const PadW = 10;
const PadH = 20;

function toPixel(percent, width) {
  return percent * width / 100;
}

const Dot = (prop) => {
  const { x, y, r, color, border, ripple } = prop;
  const transform = `translate(${x} ${y})`;
  const shadow = ripple
    ? <circle cx={0} cy={0} transform={transform} r={r} fill={color} className="fx-ripple" />
    : '';

  return (
    <g>
      {shadow}
      <circle cx={x} cy={y} r={r} fill={border} />
      <circle cx={x} cy={y} r={r - 1} fill={color} />
    </g>
  );
};


const Floor = (props) => {
  const { markers = [], name } = props;
  const viewport = [-PadW, -PadH, MapW + PadW * 2, MapH + PadH * 2].join(' ');
  const map = 'assets/floor.svg';
  const dots = markers.map((marker) => {
    const { color = 'black', text, hover, avatar, offset, above, ripple } = marker;
    let { x, y } = marker;
    x = toPixel(x, MapW);
    y = toPixel(y, MapH);
    const onClick = () => console.log('click', text);
    const label = text
      ? <Label x={x} y={y} text={text} offset={offset} above={above} onClick={onClick} />
      : '';
    let dot = <Dot x={x} y={y} r={5} color={color} border="white" ripple={ripple} />;

    const w = 16;
    if (avatar) {
      dot = <image x={x} y={y} width={w} height={w} xlinkHref={avatar} className="avatar" />;
    }

    return (
      <g key={text}>
        <title>{hover}</title>
        {dot}
        {label}
      </g>
    );
  });
  const cls = ['map'];
  if (!markers.length) cls.push('empty');
  return (
    <div className={cls.join(' ')}>
      <div className="map-name">{name}</div>
      <svg width="100%" height="200" viewBox={viewport} version="1.1">
        <image x="0" y="0" width={MapW} height={MapH} xlinkHref={map} />
        {dots}
      </svg>
    </div>
  );
};

Floor.propTypes = {
  markers: PropTypes.array,
  name: PropTypes.string,
};

module.exports = Floor;
