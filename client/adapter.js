const BASE = '/';
const ROOM_STATUS = BASE + 'rooms/status';
const PEOPLE = BASE + 'people';
const SCORES = BASE + 'scores';
const EVENTS = BASE + 'events';

function post(url, body) {
  // console.log('post', url, body);
  return fetch(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(body),
  });
}

function getJSON(url) {
  return fetch(url).then(data => data.json());
}

module.exports = {
  getRoomStatus: () => getJSON(ROOM_STATUS),
  getPeople: () => getJSON(PEOPLE),
  savePerson: (data) => post(PEOPLE, data),
  getScores: () => getJSON(SCORES),
  getEvents: () => getJSON(EVENTS),
};
