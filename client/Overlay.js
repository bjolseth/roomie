const React = require('react');

const Overlay = (props) => {
  const stopClick = (e) => e.stopPropagation();
  return (
    <div className="overlay" onClick={props.onClose}>
      {props.children}
    </div>
  );
}

module.exports = Overlay;
