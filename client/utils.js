module.exports = {
  fixFont: (number) => {
    return String(number).replace(/0/g, 'o');
  },
};
