const React = require('react');

const Feedback = require('./Feedback');

const Info = () => (
  <div className="scrollable info">
    <div className="logo" />

    <div className="section-header">What is Roomie?</div>
    <div className="row">
      Roomie lets you find (available) meeting rooms on the fly with one click. The purpose is to:
      <ul>
        <li>Demonstrate the power of the Cisco video endpoint APIs (XAPI)</li>
        <li>Be a useful mobile (m)app in the daily life of the good people at Cisco Lysaker</li>
      </ul>
    </div>

    <div className="section-header">How does it work?</div>
    <div className="row">
      Roomie is a web server that connects to many of the video systems in meeting rooms at Lysaker, such as Room Kit and MX700.

      <p />They use ultrasonic motion sensors and face detection to determine if there is anyone in a room.

      <p />Roomie accesses both on-premise and Spark systems using the same XAPI integration.
    </div>

    <div className="section-header">What about the client?</div>
    <div className="row">
      The client is a standard, mobile friendly HTML 5 web app, talking to the Roomie web server, accessible at <a>www.cs.co/roomie</a>.
      <p />Be sure to save the page to your mobile home screen for easy one-click access and proper full screen behavior.
    </div>

    <div className="section-header">What about privacy?</div>
    <div className="row">
      Roomie accesses the video system APIs with as an <i>integrator</i> user. This is a very limited user that does not have access to any personally identifiable data, only whether the system is in call, if there is motion detected etc.
    </div>

    <div className="section-header">Whats the game thingy?</div>
    <div className="row">
      During Playtime 2017, Roomie adds a gaming feature. To crowd source the effort of adding 500+ employees to the map, a highscore list is used. Each time you place an employee, you get 10 points.
      <p />The contest runs until the Christmas demo 20th December, then the winner will receive a prize.
    </div>

    <div className="section-header">Who made it?</div>
    <div className="row">
      Design by Sondre Brustad, programming by Tore Bjølseth.
      <p />Special thanks to Martin Myrseth for XAPI JavaScript SDK, Lars Kvinnesland for gamification help and Øystein Birkenes for quality assurance.
      <p />For more info, contact <a>tbjolset@cisco.com</a>.
    </div>

    <div className="section-header">Feedback and ideas</div>
    <div className="row">
      Please feel free to give feedback, suggest ideas, request features or report errors.
      <Feedback />
    </div>

  </div>
);

module.exports = Info;
