const React = require('react');

const Search = (props) => {
  const onChange = (e) => props.onSearch(e.currentTarget.value);
  const clear = () => {
    props.onSearch('');
    document.getElementById('search').blur();
  };
  const x = props.value ? <div className="clear" onClick={clear}>X</div> : '';
  const placeholder = props.placeholder || 'Search';
  return (
    <div className="search">
      <input id="search" value={props.value} onChange={onChange} placeholder={placeholder} />
      {x}
    </div>
  );
};

module.exports = Search;
