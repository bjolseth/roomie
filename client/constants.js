module.exports = {
  Color: {
    Unknown: '#999999',
    Available: '#30d557',
    Unavailable: '#ff7133',
    Static: '#07C1E4',
  },
};
