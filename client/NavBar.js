const React = require('react');

const Button = (props) => {
  const url = `./assets/${props.url}${props.selected ? '-blue' : '' }.svg`;
  const style = { backgroundImage: `url(${url})` };
  const onClick = () => props.setPage(props.page);
  const cls = 'button' + (props.selected ? ' selected' : '');

  return (
    <div onClick={onClick} className={cls} style={style} >{props.text}</div>
  );
}
const NavBar = (props) => {
  const sections = props.sections;
  const buttons = sections.map(s => {
    const sel = s === props.page;
    return <Button key={s} setPage={props.setPage} selected={sel} page={s} text={s} url={s} />;
  });
  const cls1 = props.page === 'map' ? 'selected' : '';

  return (
    <div className="nav-bar" >{buttons}</div>
  );
};

module.exports = NavBar;
