const React = require('react');

const Logo = () => (
  <div className="logo unavailable">
    r
    <div className="ring" />
  </div>
);

module.exports = Logo;
