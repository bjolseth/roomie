const React = require('react');

const MapMarker = (props) => {
  let { x, y, status, above, name } = props;
  x = typeof x === 'undefined' ? 50 : x;
  y = typeof y === 'undefined' ? 50 : y;
  const style = { left: x + '%', top: y + '%' };
  const cls = 'point ' + status;
  const cls2 = 'room-marker' + (above ? ' above' : '');
  const txtPos = { left: `-${name.length * 5}px` };
  const cls3 = 'shadow fx-ripple ' + status;
  const showRipple = status !== 'unknown';

  return (
    <div className={cls2} style={style}>
      { showRipple && <div className={cls3} /> }
      <div className={cls} />
      <div className="room-name" style={txtPos}>{name}</div>
    </div>
  );
};
module.exports = MapMarker;
