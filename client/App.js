const React = require('react');

const PollTimeSec = 10;

const FloorPlan = require('./FloorPlan');
const Info = require('./Info');
const Intro = require('./Intro');
const NavBar = require('./NavBar');
const Overlay = require('./Overlay');
const PersonList = require('./PersonList');
const RoomList = require('./RoomList');
const Stats = require('./Stats');
const getStats = require('./fakestats');
const config = require('./config');

function getSections() {
  const sections = ['map', 'rooms'];
  if (config.showPeople) sections.push('people');
  if (document.location.href.includes('dev.html')) {
    sections.push('stats');
  }
  sections.push('info');
  return sections;
}

class App extends React.Component {

  constructor(props) {
    super(props);
    const lastPage = localStorage.getItem('lastPage') || 'map';
    const userId = localStorage.getItem('userId');
    this.state = {
      status: {},
      page: lastPage,
      searchWord: '',
      people: [],
      userId,
      showIntro: false, // !localStorage.getItem('beenHereBefore'),
    };
    window.app = this;
  }

  setRoomFilter(filter) {
    this.setState({ searchWord: filter.toLowerCase() });
  }

  componentDidMount() {
    this.updateRooms();
    this.updatePeople();
    this.timer = setInterval(this.updateRooms.bind(this), PollTimeSec * 1000);
  }

  componentWillUnmount() {
    this.clearInterval(this.timer);
  }

  updatePeople() {
    this.props.adapter.getPeople()
    .then(people => this.setState({ people }));
  }

  updateRooms() {
    this.props.adapter.getRoomStatus()
    .then(status => this.setState({ status }));
  }

  render() {
    let content;
    const { page, status, searchWord, showIntro, people } = this.state;

    const setPage = (page) => {
      this.setState({ page });
      localStorage.setItem('lastPage', page);
    };
    const onFilter = this.setRoomFilter.bind(this);
    const savePerson = (data) => {
      data.savedBy = this.state.userId;
      this.props.adapter.savePerson(data)
      .then(() => this.updatePeople());
    };
    const showMap = () => setPage('map');

    switch (page) {
      case 'map':
        content = <FloorPlan onFilter={onFilter} status={status} roomFilter={searchWord}
          people={people} />;
        break;
      case 'people':
        content = <PersonList onFilter={onFilter} searchWord={searchWord} people={people}
          savePerson={savePerson} />;
        break;
      case 'stats':
        content = <Stats getStats={getStats} />;
        break;
      case 'info':
        content = <Info />;
        break;
      case 'rooms':
        content = <RoomList showMap={showMap} onFilter={onFilter} status={status}
          roomFilter={searchWord} />;
        break;
      default:
        content = <div>Nothing to see here</div>;
    }

    const hide = () => {
      this.setState({ showIntro: false });
      localStorage.setItem('beenHereBefore', 1);
    }
    const intro = <Overlay onClose={hide}><Intro /></Overlay>;
    const sections = getSections();

    return (
      <div className="screen">
        {content}
        <NavBar setPage={setPage} page={page} sections={sections} />
        {showIntro && intro}
      </div>
    );
  }

}

module.exports = App;
