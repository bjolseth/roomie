const React = require('react');

const { floors } = require('./config');

const MapMarker = require('./MapMarker');
const BackBar = require('./BackBar');

const resizeImg = (url) => {
  if (url) return url.replace('~1600', '~640');
}

const SelectFloor = (props) => {
  const list = Object.assign(floors, { 'NA': { name: 'N/A' }});
  const options = Object.keys(list).map(floor => {
    const cls = 'floor-button' + (props.floor === floor ? ' selected' : '');
    const onClick = () => props.onChange(floor);
    const name = floors[floor].name;
    return <button onClick={onClick} className={cls} key={name}>{name}</button>;
  });
  return <div value={props.floor} className="floor-name">{options}</div>;
};

const getTouchOrClickPos = (e) => {
  if (e.nativeEvent.changedTouches) return {
    x: e.nativeEvent.changedTouches[0].pageX - e.currentTarget.offsetLeft,
    y: e.nativeEvent.changedTouches[0].pageY - e.currentTarget.offsetTop,
  };
  return {
    x: e.nativeEvent.offsetX,
    y: e.nativeEvent.offsetY,
  };
}

const EditButton = (props) => {
  const cls = 'edit-button' + ( props.isEditing ? ' selected' : '');
  return <button onClick={props.onClick} className={cls} />;
};

const Map = (props) => {
  const { floor, setLocation, children, map } = props;
  const name = floors[floor] ? floors[floor].name : 'N/A';
  const floorName = <div className="floor-name">{name}</div>;
  const label = props.label || floorName;
  const onMove = (e) => {
    const pos = getTouchOrClickPos(e);
    const x = -1.5 + 100 * pos.x / e.currentTarget.clientWidth;
    const y = -2 + 100 * pos.y  / e.currentTarget.clientHeight;
    if (setLocation) setLocation({ x, y });
  };

  const help = props.label ? <div>Select level and click map to set new position</div> : '';
  const style = { backgroundImage: `url(${map})` };

  return (
    <div className="floor-box">
      {label}
      <div onClick={onMove} className="floor" style={style}>{children}</div>
      {help}
    </div>
  );
}

class PersonDetails extends React.Component {
  constructor(props) {
    super(props);
    const { person } = this.props;
    const location = { x: person.x, y: person.y };
    this.state = { edit: false, floor: person.floor, location };
  }

  toggleEdit() {
    const isEditing = this.state.edit;
    if (isEditing) this.savePerson();
    this.setState({ edit: !isEditing });
  }

  savePerson() {
    const { floor, location } = this.state;
    const person = this.props.person;
    person.floor = floor;
    person.x = location.x;
    person.y = location.y;
    this.props.savePerson(person);
  }

  render() {
    const { person } = this.props;
    const { edit, floor, location } = this.state;
    const img = './assets/persons-blue.svg';
    const setFloor = (floor) => this.setState({ floor });
    const selector = edit ? <SelectFloor floor={floor} onChange={setFloor}/> : null;
    const setLocation = edit ? (location) => this.setState({ location }) : null;
    const toggleEdit = this.toggleEdit.bind(this);
    const editButton = <EditButton onClick={toggleEdit} isEditing={edit} />
    const back = () => {
      if (!edit || confirm('Go back without saving?')) this.props.back();
    };
    const marker = floor && floor !== 'NA' ? (
      <MapMarker
        name={person.name}
        x={location.x}
        y={location.y}
        status="person-marker"
      />
    ) : null;
    const map = floors[floor] ? floors[floor].map : '';

    return [
      <BackBar key="back-bar" title="Details" onBack={back} actionButton={editButton} />,
      <div key="person-details" className="person-details scrollable">
        <Map floor={person.floor} label={selector} setLocation={setLocation} map={map}>
          {marker}
        </Map>
        <div className="info">
          <img className="avatar" src={resizeImg(person.avatar)} />
          <div className="name">
            {person.name}
            <div className="email">{person.email}</div>
          </div>
        </div>
      </div>
    ];
  }
}

module.exports = PersonDetails;
