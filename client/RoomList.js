const React = require('react');

const config = require('./config');
const Search = require('./Search');

const RoomList = (props) => {
  const { status, roomFilter } = props;

  const rooms = config.rooms.sort((r1, r2) => {
    if (r1.floor === r2.floor) return r1.name > r2.name ? 1 : -1;
    return r1.floor > r2.floor ? 1 : -1;
  });

  let previousFloor;
  const list = [];
  rooms.forEach((room) => {
    if (roomFilter && !room.name.toLowerCase().includes(roomFilter)) return;

    const data = status[room.name] || {};
    const { available, inCall, peopleCount, peoplePresence} = data;
    const noData = typeof available === 'undefined';
    const cls = noData ? '' : (available ? 'available' : 'unavailable');
    const indicator = data ? <div className={'ball ' + cls} /> : '';
    const incall = data.inCall ? <div>In-call</div> : '';
    const motion = peoplePresence ? <div>Motion</div> : '';
    const count = data.peopleCount > 0 ? <div>{data.peopleCount}+ faces</div> : '';
    const icons = (
      <div className="room-icons">
        {incall}
        {count}
        {motion}
        {indicator}
      </div>
    );

    if (previousFloor !== room.floor) list.push(
      <div key={room.floor} className="section-header">{config.floors[room.floor].name}</div>
    );
    const onFilter = () => {
      props.onFilter(room.name);
      props.showMap();
    };

    list.push((
      <div key={room.name} className="row" onClick={onFilter} >
        <div>{room.name}</div>
        {icons}
      </div>
    ));
    previousFloor = room.floor;
  });

  return [
    <Search key="search" placeholder="Search rooms" value={roomFilter} onSearch={props.onFilter} />,
    <div key="room-list" className="scrollable room-list">
      {list}
      <div className="section-info">
        Room is marked as not available if the video system is in call, or if it detects motion
        or faces.
      </div>
    </div>
  ];
};

module.exports = RoomList;
