const React = require('react');

const PersonDetails = require('./PersonDetails');
const Search = require('./Search');
const Selector = require('./Selector');
const { floors } = require('./config');

const sortAlgorithm = (p1, p2) => {
  return p1.name < p2.name ? -1 : 1;
};

const resizeImg = (url) => {
  return url.replace('~1600', '~110');
}

function floorName(floor) {
  if (!floor ) return <span className="notice">Not set</span>;
  if (floors[floor]) return floors[floor].name;
  return 'N/A';
}

class PersonList extends React.Component {

  constructor(props) {
    super(props);
    this.state = { person: null, searchWord: '', filter: 'all' };
  }

  select(person) {
    this.setState({ person });
  }

  renderList() {
    let people = this.props.people;
    const filter = this.state.filter;
    if (filter === 'placed') people = people.filter(p => p.floor);
    else if (filter === 'unknown') people = people.filter(p => !p.floor);

    let searchWord = this.state.searchWord.toLowerCase();
    const list = people.sort(sortAlgorithm).map(p => {
      if (searchWord && !p.name.toLowerCase().includes(searchWord)) return;

      const onClick = () => this.select(p);
      const floor = floorName(p.floor);
      const avatar = p.avatar ? <img className="avatar" src={resizeImg(p.avatar)} />
        : <div className="avatar" />;

      return (
        <div key={p.email} className="row persons" onClick={onClick}>
          {avatar}
          <div className="name">{p.name}</div>
          <div className="floor-label">{floor}</div>
        </div>
      );
    });
    return list;
  }

  renderPerson(person) {
    const back = () => this.setState({ person: null });
    const { savePerson } = this.props;
    return (
      <PersonDetails savePerson={savePerson} person={person} back={back} />
    );
  }

  render() {
    const { person, searchWord, filter } = this.state;
    if (person) return this.renderPerson(person);
    const onSearch = (searchWord) => this.setState({ searchWord });
    const list = this.renderList();
    const filters = {
      all: 'All', placed: 'Placed', unknown: 'Not placed',
    };
    const setFilter = (filter) => this.setState({ filter });

    return [
      <Search key="search" placeholder="Search people" value={searchWord} onSearch={onSearch} />,
      <Selector key="filter" options={filters} onChange={setFilter} value={filter} />,
      <div key="person-list" className="scrollable room-list">
        {list}
      </div>
    ];
  }

}

module.exports = PersonList;
