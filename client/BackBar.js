const React = require('react');

const BackBar = (props) => {
  return (
    <div className="back-bar">
      <button className="back-button" onClick={props.onBack} />
      {props.title}
      {props.actionButton}
    </div>
  );
};

module.exports = BackBar;
