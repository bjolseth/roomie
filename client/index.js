/* eslint-disable no-console */
const ReactDOM = require('react-dom');
const React = require('react');
const initReactFastclick = require('react-fastclick');
const App = require('./App');

const adapter = require('./adapter');

// mobiles have a 200ms latency on touch, naah
initReactFastclick();

// if manifest has been updated, auto refresh:
// const appCache = window.applicationCache;
// if (appCache.status === appCache.UPDATEREADY) window.location.reload();

/* global document */
ReactDOM.render(
  <App adapter={adapter} />,
  document.getElementById('app'),
);


if (!navigator.userAgent.toLowerCase().includes('qtwebengine')) {
  const viewPortTag = document.createElement('meta');
  viewPortTag.id = 'viewport';
  viewPortTag.name = 'viewport';
  viewPortTag.content = 'width=600, user-scalable=1';
  document.getElementsByTagName('head')[0].appendChild(viewPortTag);
  console.log('added viewport')
}
// if (location.search.includes('sparkboard')) {
//   const viewport = document.querySelector('meta[name=viewport]');
//   viewport.setAttribute('content', 'width=1024');
// }
