const React = require('react');

const config = require('./config');
const { sendMessageRoom } = require('./sparkAdapter');

class Feedback extends React.Component {
  constructor(props) {
    super(props);
    this.state = { name: '', comment: '' };
  }

  submit(name, comment) {
    const user = name || 'Anonymous';
    if (comment) {
      const msg = `${user} says: \n> ${comment}`;
      try {
        sendMessageRoom(config.sparkRoom, msg, config.botToken);
        this.setState({ comment: '' });
        alert('Thank you!');
      }
      catch(e) {
        alert('Sorry, not able to send at the moment');
      }
    }
    else {
      alert('You need to enter a comment');
    }
  }

  render() {
    const { name, comment } = this.state;
    const setName = (e) => this.setState({ name: e.currentTarget.value });
    const setComment = (e) => this.setState({ comment: e.currentTarget.value });
    const submit = () => this.submit(name, comment);

    return (
      <div className="feedback">
        <textarea onChange={setComment} value={comment}
          placeholder="Praise, contructive criticism, ideas, feature requests,..."
        />
        <input onChange={setName} value={name} placeholder="You name or Spark email (optional)" />
        <button onClick={submit}>Submit</button>
      </div>
    );
  }
}

module.exports = Feedback;
