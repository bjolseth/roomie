const React = require('react');

const config = require('./config');
const Legend = require('./Legend');
const Search = require('./Search');
const FloorSvg = require('./FloorSvg');
const { Color } = require('./constants');
const InfoBlock = require('./InfoBlock');

const MinSearchCharacters = 2;

function findRooms(floor, status, filter) {
  /* eslint-disable eqeqeq */
  const rooms = config.rooms.filter(codec => codec.floor == floor);

  // add room state for each room in config
  const markers = rooms.map((room) => {
    const { x, y, name, nameAbove, offset } = room;
    let color = Color.Unknown;
    if (filter && !name.toLowerCase().includes(filter.toLowerCase())) return null;

    const data = status[name];
    if (data) color = data.available ? Color.Available : Color.Unavailable;
    const ripple = !!data;
    return { x, y, text: name, color, above: nameAbove, offset, ripple };
  }).filter(v => v); // removes null elements

  return markers;
}

function findPeople(floor, people, filter) {
  if (filter.length < MinSearchCharacters) return [];

  const search = filter.toLowerCase();
  /* eslint-disable eqeqeq */
  const dudes = people.filter((p) => {
    if (p.floor != floor) return false;

    const name = p.name.toLowerCase();
    if (name.startsWith(filter)) return true;
    const names = name.split(' ');
    const nameMatch = names.find(n => n.startsWith(search));
    return !!nameMatch;
  });

  return dudes.map((dude) => {
    const { x = 50, y = 50, name } = dude;
    return { x, y, text: name, color: Color.Static };
  });
}

const FloorPlan = (props) => {
  const { status, roomFilter, onFilter, people = [] } = props;
  const levels = Object.keys(config.floors);
  const floors = levels.map((floor) => {
    const rooms = findRooms(floor, status, roomFilter);
    const dudes = findPeople(floor, people, roomFilter);
    const markers = rooms.concat(dudes);
    const name = config.floors[floor].name;
    return <FloorSvg key={floor} name={name} markers={markers} />;
  }).filter(v => v);
  const noResult = <div style={{ marginTop: '50px' }}>Empty result</div>;
  floors.push(<InfoBlock key="info" />);

  return ([
    <Search key="search" placeholder="Search rooms or people" onSearch={onFilter} value={roomFilter} />,
    <div key="floor-plan" className="floor-plan scrollable">
      <Legend key="legend" />
      {floors.length ? floors : noResult}
    </div>,
  ]);
};

module.exports = FloorPlan;
