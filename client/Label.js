const React = require('react');
const { number, string, bool, func } = require('prop-types');

const FontSize = 10;
const Round = 10;
const Background = 'rgba(0, 0, 0, 0.7)';
const ArrowSize = 5;

const Label = (props) => {
  const {
    x, y, text, fill, above = false, offset = 0, color = 'white', onClick,
  } = props;
  const width = text.length * 5 + 15;
  const height = FontSize * 1.7;
  const aW = ArrowSize;
  const aH = above ? -aW : aW;
  const xS = x - width / 2 + offset;
  const aOff = above ? -6 : 6;
  const arrow = `M${x - aW},${y + aH + aOff} l${aW},${-aH} l${aW},${aH} Z`;
  const y2 = y + (above ? -27.95 : 10.95);

  return (
    <g onClick={onClick}>
      <rect x={xS} y={y2} width={width} height={height} fill={fill} rx={Round} />
      <text x={xS + 6} y={y2 + 12} fill={color} fontSize={FontSize}>{text}</text>
      <path d={arrow} fill={fill} />
    </g>
  );
};


Label.propTypes = {
  x: number,
  y: number,
  fill: string,
  text: string,
  above: bool,
  offset: number,
  color: string,
  onClick: func,
};

Label.defaultProps = {
  x: 0,
  y: 0,
  fill: Background,
  text: '',
  above: false,
  offset: 0,
  color: 'white',
  onClick: null,
};

module.exports = Label;
