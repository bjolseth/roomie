const React = require('react');

const Selector = (props) => {
  const options = props.options;
  const list = Object.keys(options).map(key => {
    const cls = 'selector-button' + (props.value === key ? ' selected' : '');
    const onClick = () => props.onChange(key);
    return <button onClick={onClick} className={cls} key={key}>{options[key]}</button>;
  });
  return <div value={props.value} className="selector">{list}</div>;
};

module.exports = Selector;
