const fs = require('fs');

const ReconnectAdapter = require('./adapter');
const RoomHandler = require('./macro/roomie');
const PluginManager = require('./plugins/pluginManager');
const db = require('./db');

const ReconnectSec = 60;
const PollIntervalSec = 10;


function getRandomData(chance = 0.5) {
  const randBool = () => Math.random() < chance;
  return {
    peopleCount: Math.floor(Math.random() * 5),
    peoplePresence: randBool(),
    peoplePresenceDetector: true,
    inStandby: randBool(),
    inCall: randBool(),
    available: randBool(),
  };
}

class Controller {

  constructor(config, mock = false) {
    this.config = config;
    this.roomHandlers = new Map();
    this.roomStatus = {};
    this.pluginManager = new PluginManager();

    setInterval(() => this.pollData(), PollIntervalSec * 1000);

    if (config.dbSaveIntervalSec)
      setInterval(() => this.saveToDb(), config.dbSaveIntervalSec * 1000);

    if (mock) {
      this.setupMock(config.codecs);
      return;
    }

    config.codecs.map(codec => {
      const roomHandler = new RoomHandler();
      if (!codec.host) return;
      const adapter = new ReconnectAdapter(codec, (xapi) => roomHandler.setup(xapi));
      adapter.start(ReconnectSec);
      this.roomHandlers.set(codec.name, roomHandler);
    });

  }

  setupMock(codecs) {
    const mockHandler = {
      getData: () => getRandomData(),
    };
    this.config.codecs.map(codec => {
      this.roomHandlers.set(codec.name, mockHandler);
    });
  }

  pollData() {
    this.roomHandlers.forEach((handler, name) => {
      const data = handler.getData();
      if (!data) this.roomStatus[name] = null;
      else {
        data.room = name;
        this.roomStatus[name] = data;
      }
    });
    this.pluginManager.update(this.roomStatus);
  }

  saveToDb() {
    const file = this.config.dbFile;
    const data = [];
    Object.keys(this.roomStatus).forEach(name => {
      const room = this.roomStatus[name];
      data.push(JSON.stringify(room));
    });
    const dataStr = data.join('\n');
    fs.appendFileSync(file, dataStr);
  }

  getRoomStatus(req, res) {
    return res.json(this.roomStatus);
  }

  getPeople(req, res) {
    res.json(db.getPersons());
  }

  updatePerson(req, res) {
    res.json(db.updatePerson(req.body));
  }

  saveEvent(req, res) {
    db.saveEvent(req.body);
    res.json(req.body);
  }

  getEvents(req, res) {
    res.json(db.getEvents());
  }

  getScores(req, res) {
    res.json(db.getScores());
  }
}

module.exports = Controller;
