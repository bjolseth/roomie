const jsxapi = require('jsxapi');

/** Connection wrapper for xapi, simplifies reconnect */
class ReconnectAdapter {

  constructor(codec, onConnect) {
    this.codec = codec;
    this.xapi = null;
    this.onConnect = onConnect;
    this.timer = 0;
  }

  isConnected() {
    return this.xapi.backend.state === 'ready';
  }

  getXapi() {
    return this.xapi;
  }

  start(intervalSec = 60) {
    this.connect();
    this.timer = setInterval(() => this.verifyConnection(), intervalSec * 1000);
  }

  stop() {
    clearInterval(this.timer);
  }

  connect() {
    const { host, username, password = '' } = this.codec;
    const xapi = jsxapi.connect(host, { username, password });

    console.log('Connecting to', host);

    xapi.on('ready', () => {
      console.log('Connected to', host);
      if (this.onConnect) this.onConnect(xapi);
    })

    xapi.on('close', () => console.log(`Connection closed: ${host}`));
    xapi.on('error', () => console.log(`Connection error: ${host}`));
    this.xapi = xapi;
  }

  verifyConnection() {
    if (this.isConnected()) return;
    this.connect();
  }
}

module.exports = ReconnectAdapter;
