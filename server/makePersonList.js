const spark = require('./sparkAdapter');
const fs = require('fs');

// Connecting Norway:
const room = 'Y2lzY29zcGFyazovL3VzL1JPT00vN2YzODg2YjAtYjhlNS0xMWU0LTgxZTQtYmQyNTA5MzE0NWFh';
const personList = [];
const file = './persons.json';
const MaxPersons = 1000;

function addMemberDetails(person) {
  const email = person.emails[0];
  if (person.type !== 'person' || !email.includes('cisco.com')) return;
  const data = {
    avatar: person.avatar,
    name: person.displayName,
    email,
    created: person.created,
  };
  personList.push(data);
}

function finish() {
  fs.writeFileSync(file, JSON.stringify(personList, null, 2));
}

let i = 0;

function addNext(list, token) {
  if (i++ > MaxPersons || !list.length) {
    finish();
    return;
  }
  const item = list.pop();
  spark.findUser(item.personId, token).then(data => {
    console.log('add', i, item.personEmail);
    addMemberDetails(data);
    addNext(list, token);
  });
}

function getAllMembers(token) {
  spark.getPeople(room, token)
  .then(data => {
    const list = data.items;
    addNext(list, token);
    // console.log(JSON.stringify(emails, null, 2));
  })
  // .catch(e => console.error('Oooops', e));
}

const token = process.argv[2];
if (!token) console.log("Must auth specify token as first argument");
else getAllMembers(token);
