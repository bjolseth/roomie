
function getDate() {
  function pad(n) {return n < 10 ? '0' + n : n;}
  const now = new Date();
  const date = `${now.getFullYear()}-${pad(now.getMonth() + 1)}-${pad(now.getDate())}`;
  const time = `${pad(now.getHours())}:${pad(now.getMinutes())}:${pad(now.getSeconds())}`;
  return `${date} ${time}`;
}

class RoomHandler {

  constructor() {
    this.data = {
      peopleCount: 0,
      peoplePresence: false,
      peoplePresenceDetector: false,
      inStandby: false,
      inCall: false,
      available: null,
    };
  }

  getData() {
    if (!this.xapi || this.xapi.backend.state !== 'ready') return null;
    this.data.date = getDate();
    this.data.available = this.isAvailable();
    return this.data;
  }

  setup(xapi) {
    this.xapi = xapi;
    this.monitorRoom();
  }

  isAvailable() {
    const { inStandby, peoplePresenceDetector, peoplePresence, peopleCount, inCall } = this.data;
    if (inStandby && !peoplePresenceDetector) return true;
    if ( !inCall && !peoplePresence && peopleCount < 1) return true;
    return false;
  }

  findAndListen(path, callback, type = 'status') {
    this.xapi[type].on(path, value => callback(value));
    this.xapi[type].get(path).then(value => callback(value))
    .catch(() => console.log('N/A:', path));
  }

  monitorRoom() {
    this.findAndListen('RoomAnalytics PeoplePresenceDetector', (value) => {
      this.data.peoplePresenceDetector = value === 'On';
    }, 'config');

    this.findAndListen('RoomAnalytics PeoplePresence', (presence) => {
      this.data.peoplePresence = presence === 'Yes';
    });

    this.findAndListen('RoomAnalytics PeopleCount Current', (count) => {
      this.data.peopleCount = count;
    });

    this.findAndListen('Standby State', (state) => {
      this.data.inStandby = state === 'Standby';
    });

    this.findAndListen('SystemUnit State NumberOfActiveCalls', (calls) => {
      this.data.inCall = calls > 0;
    });
  }
}

module.exports = RoomHandler;

// To run as macro instead:
// const xapi = require('xapi');
// const handler = new RoomHandler();
// handler.setup(xapi);
