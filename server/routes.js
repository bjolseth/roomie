module.exports = function(app, controller) {

  app.route('/server/*')
    .get((req, res) => res.send('No access'));

  app.route('/rooms/status')
    .get((req, res) => controller.getRoomStatus(req, res));

  app.route('/people/')
    .get((req, res) => controller.getPeople(req, res))
    .post((req, res) => controller.updatePerson(req, res));

  app.route('/events/')
    .get((req, res) => controller.getEvents(req, res))
    .post((req, res) => controller.saveEvent(req, res));

  app.route('/scores/')
    .get((req, res) => controller.getScores(req, res));
};
