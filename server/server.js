const express = require('express');
const bodyParser = require('body-parser');
const fs = require('fs');

const routes = require('./routes');
const Controller = require('./controller');
const roomie = require('./macro/roomie');
const ReconnectAdapter = require('./adapter');

function getConfig() {
  try {
    return JSON.parse(fs.readFileSync('./server/config.json'));
  }
  catch (e) {
    console.error('You must create a valid server/config.json file. Use server/config.json.example as reference.');
    process.exit();
  }
}

const config = getConfig();
const DefaultPort = 2468;
const mock = process.env.MOCK;
const controller = new Controller(config, mock);
const app = express();
const port = config.port || DefaultPort;

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
routes(app, controller);
app.use(express.static('.'));
app.listen(port);
console.log('Running Roomie server on port', port);
