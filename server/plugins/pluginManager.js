const HuePlugin = require('./hue/huePlugin');

function clone(data) {
  return JSON.parse(JSON.stringify(data));
}

class PluginManager {

  constructor() {
    this.plugins = [];
    this.plugins.push(new HuePlugin());
  }

  update(data) {
    const copy = clone(data); // so plugins can diff with previous measurements
    this.plugins.forEach(p => p.update(copy));
  }
}

module.exports = PluginManager;
