const config = require('./hueConfig');
const LightController = require('./lightController');

class HuePlugin {

  constructor() {
    this.data = {};
    this.hueConnectors = {};
    for (const key in config) {
      const connector = new LightController();
      connector.connectHue(config[key]);
      this.hueConnectors[key] = connector;
    }
  }

  checkRoom(name, room) {
    if (!room) return; // no data yet

    const previous = this.data[name] || {};
    if (room.available !== previous.available) {
      // console.log('room changed:', name, 'available', room.available);
      if (config[name]) {
        this.updateLight(name, config[name], room.available);
      }
    }
  }

  updateLight(room, hueData, available) {
    console.log('Change hue available light', room, available);
    const connector = this.hueConnectors[room];
    if (!connector) {
      console.error('No hue connector for', room);
      return;
    }

    const color = available ? connector.colors.green : connector.colors.red;
    connector.setColor(color);
  }

  update(roomData) {
    const list = Object.keys(roomData);
    list.forEach((name) => {
      this.checkRoom(name, roomData[name]);
      this.data = roomData;
    });
  }
}

module.exports = HuePlugin;
