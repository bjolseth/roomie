const { HueApi } = require('node-hue-api');
const hueBase = require('node-hue-api');

const BridgeSearchTimeout = 30 * 1000;
const HueUserTitle = 'roomie-hue-user';

const Colors = {
  red: { bri: 139, hue: 432, sat: 249 },
  orange: { bri: 254, hue: 10046, sat: 254 },
  green: { bri: 120, hue: 24264, sat: 254 },
};

class LightController {

  constructor() {
    this.colors = Colors;
  }

  connectHue({ host, username, pollTime }) {
    console.log('Connecting to hue:', host, username);
    this.hue = new HueApi(host, username);
    clearInterval(this.pollTimer);
    if (pollTime) {
      this.pollTimer = setInterval(this.poll.bind(this), pollTime);
    }
  }

  poll() {
    this.hue.lights().then((data) => {
      try {
        const light = data.lights[0].state;
        const { reachable } = light; // false if light not plugged in
        const value = reachable ? light.bri : 0;
        const on = reachable ? light.on : false;
        console.log(light);
        // console.log('first light', on, value);
      } catch (e) {
        console.log('not able to find any lights');
      }
    });
  }

  setBrightness(bri) {
    if (bri < 5) return this.setPower(false);
    else return this.setLightState({ on: true, bri, transitiontime: 1 });
  }

  setColor(color) {
    return this.setLightState(color);
  }

  setPower(on) {
    return this.setLightState({ on });
  }

  blink(times = 3) {
    for (let i = 0; i < 2 * times + 1; i += 1) {
      const on = i % 2 === 0;
      setTimeout(() => this.setLightState({ on, transitiontime: 1 }), i * 300);
    }
  }

  setLight(on) {
    return this.setLightState({ on });
  }

  setLightState(state) {
    return this.hue.setGroupLightState(0, state);
  }

  findBridge(callback) {
    hueBase.upnpSearch(BridgeSearchTimeout)
      .then((bridge) => {
        if (bridge.length) callback(bridge[0].ipaddress);
        else callback(null);
      })
      .fail(() => callback(null))
      .done();
  }

  createUser(callback) {
    const host = this.hue._config.hostname;
    new HueApi().registerUser(host, HueUserTitle)
      .then(callback)
      .fail(() => callback(false))
      .done();
  }

}

module.exports = LightController;
