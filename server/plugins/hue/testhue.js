const LightController = require('./lightController');
const conf = require('./hueConfig.json');

const hue = new LightController();
hue.connectHue(conf.Toyshop);

hue.poll();
hue.setColor(hue.colors.red)
.then(() => hue.poll())
.catch(e => console.log('error', e));
