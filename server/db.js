const fs = require('fs');
const personFile = './db/persons.json';
const eventFile = './db/events.json';
const scoreFile = './db/scores.json';

if (!fs.existsSync('./db')){
  fs.mkdirSync('./db');
}

function readFile(name, defaultEmpty = []) {
  let data = defaultEmpty;
  try {
    data = JSON.parse(fs.readFileSync(name));
  } catch (e) {}
  return data;
}

function saveFile(name, json) {
  fs.writeFile(name, JSON.stringify(json, null, 2));
}

function getPersons() {
  return readFile(personFile);
}

function saveScore(userId, score) {
  const list = readFile(scoreFile, {});
  const previous = list[userId] || 0;
  list[userId] = previous + parseInt(score);
  saveFile(scoreFile, list);
}

function getScores() {
  return readFile(scoreFile);
}

function updatePerson(person) {
  const list = getPersons();
  const item = list.find(p => p.email === person.email);
  if (item && !item.floor) {
    const theKing = 'gbjonteg@cisco.com';
    const score = person.email === theKing ? 250 : 50;
    saveScore(person.savedBy, score);
    saveEvent({ type: 'locate', located: person.email, userId: person.savedBy, score });
  }

  const filtered = list.filter(p => p.email !== person.email);
  filtered.push(person);
  saveFile(personFile, filtered);
  return filtered;
}

function getEvents(timeLimit = 60 * 1000) {
  const now = new Date().getTime();

  if (!timeLimit) return readFile(eventFile);
  return readFile(eventFile).filter(e => now - e.time < timeLimit);
}

function saveEvent(event) {
  const events = getEvents(0);
  event.time = new Date().getTime();
  events.push(event);
  saveFile(eventFile, events);
}

module.exports = {
  getPersons,
  updatePerson,
  saveEvent,
  getEvents,
  getScores,
};
