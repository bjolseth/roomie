const fetch = require('node-fetch');

const url = 'https://api.ciscospark.com/v1/people/';
const msgUrl = 'https://api.ciscospark.com/v1/messages/';
const getRoomsUrl = 'https://api.ciscospark.com/v1/rooms?max=100';
const membershipUrl = 'https://api.ciscospark.com/v1/memberships';

const ValidUserIdLength = 36;

// function reduceSize(url) {
// 40, 50, 80, 110, 640 and 1600.
//   if (url) return url.replace('~1600', '~640');
// }

function doIt(token, uri, method = 'GET', body = null) {
  const bearer = `Bearer ${token}`;
  /* global Headers */
  const headers = {
    'Content-Type': 'application/json; charset=utf-8',
    Authorization: `Bearer ${token}`,
  }
  const payload = body ? { method, headers, body } : { method, headers };
  return fetch(uri, payload)
  .then(r => r.json())
  .catch(e => console.error(e));
}

function findUser(userId, token) {
  // hack for mock mode:
  if (userId.length < ValidUserIdLength) {
    const mock = { avatar: `./assets/${userId.toLowerCase()}.jpeg` };
    return new Promise(resolve => resolve(mock));
  }

  const uri = `${url}/${userId}`;
  return doIt(token, uri);
}

function sendMessage(userId, markdown, token) {
  console.log('Send spark msg:', userId, markdown.substr(0, 30) + '...');
  if (userId.length < ValidUserIdLength) return false;

  const body = JSON.stringify({ toPersonId: userId, markdown });
  return doIt(token, msgUrl, 'POST', body);
}

function sendMessageEmail(email, markdown, token) {
  console.log('send spark msg:', email, markdown.substr(0, 30) + '...');
  if (!email.includes('@')) return false;

  const body = JSON.stringify({ toPersonEmail: email, markdown });
  return doIt(token, msgUrl, 'POST', body);
}


function getRooms(token) {
  return doIt(token, getRoomsUrl);
}

function getMessages(roomId, token) {
  const uri = `${msgUrl}?roomId=${roomId}`;
  return doIt(token, uri);
}

function getPeople(roomId, token) {
  const uri = `${membershipUrl}?roomId=${roomId}&max=1000`;
  return doIt(token, uri);
}

module.exports = { findUser, sendMessage, sendMessageEmail, getRooms, getMessages, getPeople };
